const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

require('es6-promise').polyfill();
require('isomorphic-fetch');

app.get('/', async (req, res) => {
  let resource = [];
  try {

    resource = await fetch(`https://jsonplaceholder.typicode.com/users/`)
      .then((res) => {
        return res.json();
      });
  } catch (e) {
    return e;
  }
  res.send(resource)
});

app.listen(port, () => {
  console.log(`Listening to ${port}`)
});
