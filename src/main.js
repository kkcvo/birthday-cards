import Vue from 'vue'
import App from './App'

import Vue2TouchEvents from 'vue2-touch-events'

Vue.use(Vue2TouchEvents)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App)
})

var quote = new Vue({
  el: '#qoute',
  data: {
    toggle: 0,
    view: 'q1',
    isVisible: true,
    quote1: '',
    quote2: '',
    quotes: ["Count your life by smiles, not tears. Count your age by friends, not years. Happy birthday!",
      "Happy birthday! I hope all your birthday wishes and dreams come true.",
      "A wish for you on your birthday, whatever you ask may you receive, whatever you seek may you find, whatever you wish may it be fulfilled on your birthday and always. Happy birthday!",
      "Another adventure filled year awaits you. Welcome it by celebrating your birthday with pomp and splendor. Wishing you a very happy and fun-filled birthday!",
      "May the joy that you have spread in the past come back to you on this day. Wishing you a very happy birthday!",
      "Happy birthday! Your life is just about to pick up speed and blast off into the stratosphere. Wear a seat belt and be sure to enjoy the journey. Happy birthday!"
    ]
  },
  ready: function() {
    var random_no = 0;
    random_no = Math.floor(Math.random() * this.quotes.length);
    this.quote1 = this.quotes[random_no];
    random_no = Math.floor(Math.random() * this.quotes.length);
    this.quote2 = this.quotes[random_no];
  },
  computed: {
    urlQuote: function() {
      return encodeURI(this.quote1);
    },
  },
  components: {
    'q1': {
      props: { dataQuote1: String },
      template: '<p>{{ dataQuote1 }}</p>'
    },
    'q2': {
      props: { dataQuote2: String },
      template: '<p>{{ dataQuote2 }}</p>'
    }
  },
  methods: {
    newQuote: function() {
      var icon = document.getElementById('rotateRefresh');
      var random_no = 0;
      this.toggle = !this.toggle;
      this.toggle == 0 ? this.view = 'q1': this.view = 'q2';

      random_no = Math.floor(Math.random() * this.quotes.length);
      this.quote1 = this.quotes[random_no];
      random_no = Math.floor(Math.random() * this.quotes.length);
      this.quote2 = this.quotes[random_no];

      icon.classList.add('animate');
      setTimeout(function(){
        icon.classList.remove('animate');
      }, 400);
    }
  }
})
